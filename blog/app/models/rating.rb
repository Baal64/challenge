class Rating < ApplicationRecord
  belongs_to :article
  validates :score  , presence: true,
                      length: { minimum: 1 }

  validates :score  , numericality: true                  
  validates :score  , numericality: { only_integer: true }
  validates :score  , numericality: { greater_than_or_equal_to: 1 }
  validates :score  , numericality: { less_than_or_equal_to: 5 }
end
