class RatingsController < ApplicationController

  http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

  def create
    @article = Article.find(params[:article_id])
    @rating = @article.ratings.create(rating_params)
    redirect_to articles_path(@articles)
    
  end

  def destroy
    @article = Article.find(params[:article_id])
    @rating = @article.ratings.find(params[:id])
    @rating.destroy
    redirect_to article_path(@article)
  end
  
  private
    def rating_params
      params.require(:rating).permit(:score)
    end
end
